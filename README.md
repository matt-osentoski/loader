# README #

### What is this repository for? ###

* This application is a stand-alone Java loader script for storing stock and asset pricing data into a data store
* 1.0.0

### Initial configuration ###
Update the /main/resources/config.properties file to update the databases and other properties


### Running the data loader (All jobs) ###
(Linux)
java -cp "target/dependency-jars/*:target/data-loader-1.0.0.jar" com.garagebandhedgefund.data.App -L all
(Windows)
java -cp "target/dependency-jars/*;target/data-loader-1.0.0.jar" com.garagebandhedgefund.data.App -L all

### Data sources ###
# Stocks
http://www.nasdaq.com/screening/company-list.aspx

# Stock Prices
http://finance.yahoo.com

# Fundamental data
http://www.stockpup.com/data/


### Running stand-alone Jobs ###
java -cp "target/dependency-jars/*;target/data-loader-1.0.0.jar" org.springframework.batch.core.launch.support.CommandLineJobRunner applicationContext.xml <JOB_NAME>

(NOTE: in MySQL, you might have to increase the max packet size by entering the following from the Mysql client:
SET GLOBAL max_allowed_packet = 1024*1024*14;
)