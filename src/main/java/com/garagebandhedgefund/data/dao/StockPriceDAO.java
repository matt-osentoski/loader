package com.garagebandhedgefund.data.dao;

/**
 * Created by Matt on 10/19/2014.
 */
public interface StockPriceDAO {

    /**
     * Delete all stock prices
     */
    public void deleteAllStockPrices();

    /**
     * Create an index on the 'symbol' property
     */
    public void createStockPriceSymbolIndex();
}
