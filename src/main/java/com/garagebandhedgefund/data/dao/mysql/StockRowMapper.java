package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.trading.model.Stock;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 10/10/14
 * Time: 3:31 PM
 */
public class StockRowMapper implements RowMapper<Stock> {
    @Override
    public Stock mapRow(ResultSet rs, int i) throws SQLException {
        Stock stock = new Stock();
        stock.setId(rs.getInt("id"));
        stock.setSymbol(rs.getString("symbol"));
        stock.setName(rs.getString("name"));
        stock.setDescription(rs.getString("description"));
        stock.setMarketCap(rs.getLong("market_cap"));
        stock.setIpoYear(rs.getString("ipo_year"));
        stock.setSector(rs.getString("sector"));
        stock.setIndustry(rs.getString("industry"));
        return stock;
    }
}
