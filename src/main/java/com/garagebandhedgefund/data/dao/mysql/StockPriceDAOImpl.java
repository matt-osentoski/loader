package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.data.dao.StockPriceDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by Matt on 10/19/2014.
 */
public class StockPriceDAOImpl implements StockPriceDAO {

    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void deleteAllStockPrices() {
        String sql = "DELETE FROM stock_prices";
        jdbcTemplate.execute(sql);
    }

    @Override
    public void createStockPriceSymbolIndex() {
        boolean exists = indexExists();
        if (exists) {
            deleteIndex();
        }
        String sql = "CREATE INDEX idx_stock_prices_symbol ON stock_prices (symbol)";
        jdbcTemplate.execute(sql);
    }

    private boolean indexExists() {
        String indexName = "idx_stock_prices_symbol";
        String sql = "SHOW INDEX FROM stock_prices WHERE KEY_NAME = ?";
        SqlRowSet srs = jdbcTemplate.queryForRowSet(sql, new Object[] {indexName});
        int rowCount = 0;
        while(srs.next()) {
            rowCount++;
        }
        if (rowCount == 0) {
            return false;
        } else {
            return true;
        }
    }

    private void deleteIndex() {
        String sql = "ALTER TABLE stock_prices DROP INDEX idx_stock_prices_symbol";
        jdbcTemplate.execute(sql);
    }
}
