package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.trading.model.Treasury;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 10/10/14
 * Time: 3:31 PM
 */
public class TreasuryRowMapper implements RowMapper<Treasury> {
    @Override
    public Treasury mapRow(ResultSet rs, int i) throws SQLException {
        Treasury t = new Treasury();
        t.setId(rs.getString("id"));
        t.setTimePeriod(rs.getDate("time_period"));
        t.setC1month(rs.getDouble("1_month"));
        t.setC3month(rs.getDouble("3_month"));
        t.setC6month(rs.getDouble("6_month"));
        t.setC1year(rs.getDouble("1_year"));
        t.setC2year(rs.getDouble("2_year"));
        t.setC3year(rs.getDouble("3_year"));
        t.setC5year(rs.getDouble("5_year"));
        t.setC7year(rs.getDouble("7_year"));
        t.setC10year(rs.getDouble("10_year"));
        t.setC20year(rs.getDouble("20_year"));
        t.setC30year(rs.getDouble("30_year"));
        return t;
    }
}
