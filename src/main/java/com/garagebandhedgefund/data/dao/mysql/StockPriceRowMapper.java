package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.trading.model.Stock;
import com.garagebandhedgefund.trading.model.StockPrice;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 10/10/14
 * Time: 3:31 PM
 */
public class StockPriceRowMapper implements RowMapper<StockPrice> {
    @Override
    public StockPrice mapRow(ResultSet rs, int i) throws SQLException {
        StockPrice sp = new StockPrice();
        sp.setId(rs.getInt("id"));
        sp.setSymbol(rs.getString("symbol"));
        sp.setPriceDate(rs.getDate("price_date"));
        sp.setOpeningPrice(rs.getDouble("opening_price"));
        sp.setHighPrice(rs.getDouble("high_price"));
        sp.setLowPrice(rs.getDouble("low_price"));
        sp.setClosingPrice(rs.getDouble("closing_price"));
        sp.setAdjClosingPrice(rs.getDouble("adj_closing_price"));
        sp.setPeriod(rs.getString("period"));
        sp.setVolume(rs.getInt("volume"));
        return sp;
    }
}
