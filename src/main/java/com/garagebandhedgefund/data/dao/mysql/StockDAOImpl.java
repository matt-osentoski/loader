package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.data.dao.StockDAO;
import com.garagebandhedgefund.trading.model.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 10/10/14
 * Time: 1:29 PM
 */
public class StockDAOImpl implements StockDAO {

    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Stock> getStocks() {
        String sql = "SELECT * FROM stocks";
        List<Stock> stocks = jdbcTemplate.query(sql, new StockRowMapper());
        return stocks;
    }

    @Override
    public List<Stock> getStocks(String exchange) {
        String sql = "SELECT * FROM stocks WHERE stock_index = ?";
        List<Stock> stocks = jdbcTemplate.query(sql, new Object[] {exchange} , new StockRowMapper());
        return stocks;
    }

    @Override
    public List<Stock> getLargeCapStocks(int count) {
        String sql = "SELECT * FROM stocks ORDER BY market_cap DESC LIMIT ?";
        List<Stock> stocks = jdbcTemplate.query(sql, new Object[] {count} , new StockRowMapper());
        return stocks;
    }

    @Override
    public void deleteAllStocks() {
        String sql = "DELETE FROM stocks";
        jdbcTemplate.execute(sql);
    }

    @Override
    public void deleteExceptLargeCap(int count) {
        String sql = "DELETE FROM stocks WHERE id NOT IN (SELECT id FROM (SELECT id FROM stocks ORDER BY market_cap desc LIMIT " + count + " ) AS x)";
        jdbcTemplate.execute(sql);
    }
}
