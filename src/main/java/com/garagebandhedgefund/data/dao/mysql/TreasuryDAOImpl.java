package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.data.dao.TreasuryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by Matt on 10/20/2014.
 */
public class TreasuryDAOImpl implements TreasuryDAO {

    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void deleteAllTreasuryEntries() {
        String sql = "DELETE FROM treasuries";
        jdbcTemplate.execute(sql);
    }
}
