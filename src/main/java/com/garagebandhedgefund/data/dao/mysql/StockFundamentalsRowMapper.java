package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.trading.model.StockFundamentalData;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 10/10/14
 * Time: 3:31 PM
 */
public class StockFundamentalsRowMapper implements RowMapper<StockFundamentalData> {
    @Override
    public StockFundamentalData mapRow(ResultSet rs, int i) throws SQLException {
        StockFundamentalData sf = new StockFundamentalData();
        sf.setId(rs.getInt("id"));
        sf.setSymbol(rs.getString("symbol"));
        sf.setQuarterEndDate(rs.getDate("quarter_end_date"));
        sf.setShares(rs.getLong("shares"));
        sf.setSharesSplitAdjusted(rs.getLong("shares_split_adj"));
        sf.setSplitFactor(rs.getInt("split_factor"));
        sf.setAssets(rs.getLong("assets"));
        sf.setLiabilities(rs.getLong("liabilities"));
        sf.setShareholderEquity(rs.getLong("shareholder_equity"));
        sf.setNonControllingInterest(rs.getLong("non_controlling_interest"));
        sf.setPreferredEquity(rs.getLong("preferred_equity"));
        sf.setGoodwillAndTangibles(rs.getLong("goodwill_and_tangibles"));
        sf.setLongTermDebt(rs.getLong("long_term_debt"));
        sf.setRevenue(rs.getLong("revenue"));
        sf.setEarnings(rs.getLong("earnings"));
        sf.setEarningsAvailForCommonStockholders(rs.getLong("earnings_avail_for_comm_stkholders"));
        sf.setEpsBasic(rs.getDouble("eps_basic"));
        sf.setEpsDiluted(rs.getDouble("eps_diluted"));
        sf.setDividendPerShare(rs.getDouble("dividend_per_share"));
        sf.setPrice(rs.getDouble("price"));
        sf.setPriceHigh(rs.getDouble("price_high"));
        sf.setPriceLow(rs.getDouble("price_low"));
        sf.setRoe(rs.getDouble("roe"));
        sf.setRoa(rs.getDouble("roa"));
        sf.setBookValueOfEquityPerShare(rs.getDouble("book_val_of_equity_per_share"));
        sf.setPbRatio(rs.getDouble("p_b_ratio"));
        sf.setPeRatio(rs.getDouble("p_e_ratio"));
        sf.setCumulativeDividends(rs.getDouble("cumulative_dividends"));
        sf.setDividendPayoutRatio(rs.getDouble("dividend_payout_ratio"));
        sf.setLongTermDebtToEquityRatio(rs.getDouble("long_term_debt_to_eqty_ratio"));
        sf.setEquityToAssetsRatio(rs.getDouble("equity_to_assets_ratio"));
        sf.setNetMargin(rs.getDouble("net_margin"));
        sf.setAssetTurnover(rs.getDouble("asset_turnover"));
        return sf;
    }
}
