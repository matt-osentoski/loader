package com.garagebandhedgefund.data.dao.mysql;

import com.garagebandhedgefund.data.dao.StockFundamentalsDAO;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by Matt on 1/9/2016.
 */
public class StockFundamentalsDAOImpl implements StockFundamentalsDAO {

    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void deleteAllStockFundamentals() {
        String sql = "DELETE FROM stock_fundamental_data";
        jdbcTemplate.execute(sql);
    }

    @Override
    public void createStockFundamentalsSymbolIndex() {
        boolean exists = indexExists();
        if (exists) {
            deleteIndex();
        }
        String sql = "CREATE INDEX idx_stock_fundamentals_symbol ON stock_fundamental_data (symbol)";
        jdbcTemplate.execute(sql);
    }

    private boolean indexExists() {
        String indexName = "idx_stock_fundamentals_symbol";
        String sql = "SHOW INDEX FROM stock_fundamental_data WHERE KEY_NAME = ?";
        SqlRowSet srs = jdbcTemplate.queryForRowSet(sql, new Object[] {indexName});
        int rowCount = 0;
        while(srs.next()) {
            rowCount++;
        }
        if (rowCount == 0) {
            return false;
        } else {
            return true;
        }
    }

    private void deleteIndex() {
        String sql = "ALTER TABLE stock_fundamental_data DROP INDEX idx_stock_fundamentals_symbol";
        jdbcTemplate.execute(sql);
    }
}
