package com.garagebandhedgefund.data.dao;

import com.garagebandhedgefund.trading.model.Stock;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 10/10/14
 * Time: 1:29 PM
 */
public interface StockDAO {

    /**
     * Retrieve all stocks
     * @return List of all Stock objects
     */
    public List<Stock> getStocks();

    /**
     * Return all stocks for a specific Exchange
     * @param exchange Exchange name
     * @return List of stocks associated with an exchange
     */
    public List<Stock> getStocks(String exchange);

    /**
     * Return all stocks for a specific Exchange
     * @param count Number of stocks to return
     * @return List of stocks associated with an exchange
     */
    public List<Stock> getLargeCapStocks(int count);

    /**
     * Delete all stocks
     */
    public void deleteAllStocks();


    /**
     * Delete all but a certain number of large cap stocks
     * @param count The number of stocks to keep.
     */
    public void deleteExceptLargeCap(int count);
}
