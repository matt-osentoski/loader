package com.garagebandhedgefund.data.dao;

/**
 * Created by Matt on 10/19/2014.
 */
public interface StockFundamentalsDAO {

    /**
     * Delete all stock prices
     */
    void deleteAllStockFundamentals();

    /**
     * Create an index on the 'symbol' property
     */
    void createStockFundamentalsSymbolIndex();
}
