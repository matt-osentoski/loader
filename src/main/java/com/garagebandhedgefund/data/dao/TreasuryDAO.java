package com.garagebandhedgefund.data.dao;

/**
 * Created by Matt on 10/19/2014.
 */
public interface TreasuryDAO {

    /**
     * Delete all treasury entries.
     */
    public void deleteAllTreasuryEntries();
}
