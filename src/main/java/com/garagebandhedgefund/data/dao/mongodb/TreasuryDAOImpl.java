package com.garagebandhedgefund.data.dao.mongodb;

import com.garagebandhedgefund.data.Constants;
import com.garagebandhedgefund.data.dao.TreasuryDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by Matt on 10/27/2014.
 */
public class TreasuryDAOImpl implements TreasuryDAO {

    private MongoTemplate mongoTemplate;

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void deleteAllTreasuryEntries() {
        mongoTemplate.dropCollection(Constants.TREASURIES_COLLECTION);
    }
}
