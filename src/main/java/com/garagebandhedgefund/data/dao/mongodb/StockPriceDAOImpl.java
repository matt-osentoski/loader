package com.garagebandhedgefund.data.dao.mongodb;

import com.garagebandhedgefund.data.Constants;
import com.garagebandhedgefund.data.dao.StockPriceDAO;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;

/**
 * Created by Matt on 10/27/2014.
 */
public class StockPriceDAOImpl implements StockPriceDAO {

    private MongoTemplate mongoTemplate;

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void deleteAllStockPrices() {
        mongoTemplate.dropCollection(Constants.STOCK_PRICES_COLLECTION);
    }

    @Override
    public void createStockPriceSymbolIndex() {
        mongoTemplate.indexOps("stock_prices").ensureIndex(new Index().on("symbol", Sort.Direction.ASC));
    }
}
