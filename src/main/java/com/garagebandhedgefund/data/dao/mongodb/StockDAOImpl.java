package com.garagebandhedgefund.data.dao.mongodb;

import com.garagebandhedgefund.data.Constants;
import com.garagebandhedgefund.data.dao.StockDAO;
import com.garagebandhedgefund.trading.model.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.List;

/**
 * Created by Matt on 10/27/2014.
 */
public class StockDAOImpl implements StockDAO {

    private MongoTemplate mongoTemplate;

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<Stock> getStocks() {

        //TODO Add mongo implementation
        throw new RuntimeException("This method hasn't been implemented.  See the TODO in the code");
    }

    @Override
    public List<Stock> getStocks(String exchange) {
        //TODO Add mongo implementation
        throw new RuntimeException("This method hasn't been implemented.  See the TODO in the code");
    }

    @Override
    public List<Stock> getLargeCapStocks(int count) {
        //TODO Add mongo implementation
        throw new RuntimeException("This method hasn't been implemented.  See the TODO in the code");
    }

    @Override
    public void deleteAllStocks() {
        mongoTemplate.dropCollection(Constants.STOCKS_COLLECTION);
    }

    @Override
    public void deleteExceptLargeCap(int count) {
        //TODO implement this method
        throw new RuntimeException("This method hasn't been implemented.  See the TODO in the code");
    }
}
