package com.garagebandhedgefund.data.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Matt on 12/31/2015.
 */
public class CurrencyUtils {

    /**
     * Returns the numeric value of a String representing a stock's market cap
     * For example: $1.23M  would be converted into 1230000
     * @param marketCap String representing a stock's market cap
     * @return Numeric value of the market cap
     */
    public static double parseMarketCapString(String marketCap) {

        if (marketCap == null || "".equals(marketCap.trim()) || "n/a".equals(marketCap)) {
            return 0;
        }

        double ret = 0;
        marketCap = marketCap.replace(",", "");
        Pattern p = Pattern.compile("(?!=\\d\\.\\d\\.)([\\d.]+)");
        Matcher m = p.matcher(marketCap);
        while(m.find()) {
            ret = Double.parseDouble(m.group(1));
        }

        // Look for characters denoting million or billion.
        if (marketCap.indexOf('M') != -1) {
            ret *= 1000000;
        } else if (marketCap.indexOf('B') != -1) {
            ret *= 1000000000;
        }
        return ret;
    }
}
