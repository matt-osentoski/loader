package com.garagebandhedgefund.data.loaders.stocks;

import com.garagebandhedgefund.data.dao.StockDAO;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Matt on 10/12/2014.
 */
public class TruncateStocksTasklet implements Tasklet {

    private StockDAO stockDAO;

    public StockDAO getStockDAO() {
        return stockDAO;
    }

    public void setStockDAO(StockDAO stockDAO) {
        this.stockDAO = stockDAO;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        stockDAO.deleteAllStocks();
        return RepeatStatus.FINISHED;
    }
}
