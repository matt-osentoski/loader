package com.garagebandhedgefund.data.loaders.stocks;

import com.garagebandhedgefund.data.dao.StockDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Matt on 10/12/2014.
 */
@Named
public class DeleteSmallCapStocksTasklet implements Tasklet {

    final static Logger logger = LoggerFactory.getLogger(DeleteSmallCapStocksTasklet.class);
    private final static int DEFAULT_STOCK_COUNT = 2000;

    @Value("${stock.price.loader.count}")
    private String stockCount;

    private int getStockCount() {
        int count = DEFAULT_STOCK_COUNT;
        if (stockCount != null) {
            try {
                count = Integer.parseInt(stockCount);
            } catch (NumberFormatException e) {
                logger.warn(e.getMessage(), e);
            }
        }
        return count;
    }

    @Inject
    public StockDAO stockDAO;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        stockDAO.deleteExceptLargeCap(getStockCount());
        return RepeatStatus.FINISHED;
    }
}
