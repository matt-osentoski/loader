package com.garagebandhedgefund.data.loaders.stocks;

import com.garagebandhedgefund.data.Constants;
import com.garagebandhedgefund.data.exceptions.InvalidStockException;
import com.garagebandhedgefund.trading.model.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;
import javax.inject.Named;

/**
 * Created by Matt on 9/21/2014.
 */
@Named
public class StockItemProcessor implements ItemProcessor<Stock, Stock> {

    final static Logger logger = LoggerFactory.getLogger(StockItemProcessor.class);

    private String stepName;
    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        this.stepName = stepExecution.getStepName();
    }


    @Override
    public Stock process(Stock stock) throws Exception {
        System.out.println("Processing: " + stock.getSymbol());

        //Skip invalid stocks
        if (stock.getSymbol() != null && (stock.getSymbol().contains("^") ||
                stock.getSymbol().contains("/") || stock.getMarketCap() == 0)) {
            logger.info("Skipping: " + stock.getSymbol());
            throw new InvalidStockException("Skipping: " + stock.getSymbol());
        }

        if (Constants.LOAD_NASDAQ_STOCKS_STEP.equals(this.stepName)) {
            stock.setStockIndex(Constants.EXCHANGE_NASDAQ);
        } else if (Constants.LOAD_NYSE_STOCKS_STEP.equals(this.stepName)) {
            stock.setStockIndex(Constants.EXCHANGE_NYSE);
        }
        return stock;
    }
}
