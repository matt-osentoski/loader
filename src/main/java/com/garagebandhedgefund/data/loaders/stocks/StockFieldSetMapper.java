package com.garagebandhedgefund.data.loaders.stocks;

import com.garagebandhedgefund.data.utils.CurrencyUtils;
import com.garagebandhedgefund.trading.model.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * Created by Matt on 12/27/2015.
 */
public class StockFieldSetMapper implements FieldSetMapper<Stock> {

    final static Logger logger = LoggerFactory.getLogger(StockFieldSetMapper.class);
    @Override
    public Stock mapFieldSet(FieldSet fieldSet) throws BindException {
        Stock stock = new Stock();
        String marketCapStr = fieldSet.readString("marketCap");
        double marketCap = CurrencyUtils.parseMarketCapString(marketCapStr);

        stock.setSymbol(fieldSet.readString("symbol"));
        stock.setName(fieldSet.readString("name"));
        stock.setMarketCap(marketCap);
        stock.setIpoYear(fieldSet.readString("ipoYear"));
        stock.setSector(fieldSet.readString("sector"));
        stock.setIndustry(fieldSet.readString("industry"));

        return stock;
    }
}
