package com.garagebandhedgefund.data.loaders;

import com.garagebandhedgefund.data.Constants;
import com.garagebandhedgefund.data.dao.StockDAO;
import com.garagebandhedgefund.trading.model.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Matt on 9/20/2014.
 */
@Named
public class LoadProcessor {

    final static Logger logger = LoggerFactory.getLogger(LoadProcessor.class);

    @Inject
    public JobLauncher jobLauncher;

    @Inject
    public Job stockLoaderJob;

    @Inject
    public Job stockPriceLoaderJob;

    @Inject
    public Job stockFundamentalsLoaderJob;

    @Inject
    public Job treasuryLoaderJob;

    @Inject
    public StockDAO stockDAO;

    public void process(String loaderType) {
        logger.info("Processing!");
        Constants.LoaderEnum enumVal = Constants.LoaderEnum.valueOf(loaderType.toUpperCase());
        switch (enumVal) {
            case STOCK: processStocks(); break;
            case STOCKFUNDAMENTALS: processStockFundamentals(); break;
            case STOCKPRICE: processStockPrices(); break;
            case TREASURY: processTreasuries(); break;
            case ALL: processAll(); break;
        }
    }

    private void processStocks() {
        logger.info("Processing Stocks!");

        try {
            JobExecution execution = jobLauncher.run(stockLoaderJob, new JobParameters());
            logger.info("Exit Status : " + execution.getStatus());

        } catch (JobExecutionAlreadyRunningException e) {
            e.printStackTrace();
        } catch (JobRestartException e) {
            e.printStackTrace();
        } catch (JobInstanceAlreadyCompleteException e) {
            e.printStackTrace();
        } catch (JobParametersInvalidException e) {
            e.printStackTrace();
        }

        logger.info("Finished processing the Stock loader");
    }

    private void processStockFundamentals() {
        logger.info("Processing Stock Fundamentals!");

        try {
            JobExecution execution = jobLauncher.run(stockFundamentalsLoaderJob, new JobParameters());
            logger.info("Exit Status : " + execution.getStatus());

        } catch (JobExecutionAlreadyRunningException e) {
            logger.error(e.getMessage(), e);
        } catch (JobRestartException e) {
            logger.error(e.getMessage(), e);
        } catch (JobInstanceAlreadyCompleteException e) {
            logger.error(e.getMessage(), e);
        } catch (JobParametersInvalidException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void processStockPrices() {
        logger.info("Processing Stock Prices!");

        try {
            JobExecution execution = jobLauncher.run(stockPriceLoaderJob, new JobParameters());
            logger.info("Exit Status : " + execution.getStatus());

        } catch (JobExecutionAlreadyRunningException e) {
            logger.error(e.getMessage(), e);
        } catch (JobRestartException e) {
            logger.error(e.getMessage(), e);
        } catch (JobInstanceAlreadyCompleteException e) {
            logger.error(e.getMessage(), e);
        } catch (JobParametersInvalidException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void processTreasuries() {
        logger.info("Processing Treasury Rates!");

        try {
            JobExecution execution = jobLauncher.run(treasuryLoaderJob, new JobParameters());
            logger.info("Exit Status : " + execution.getStatus());

        } catch (JobExecutionAlreadyRunningException e) {
            logger.error(e.getMessage(), e);
        } catch (JobRestartException e) {
            logger.error(e.getMessage(), e);
        } catch (JobInstanceAlreadyCompleteException e) {
            logger.error(e.getMessage(), e);
        } catch (JobParametersInvalidException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void processAll() {
        logger.info("Processing All!");
        processStocks();

        processStockFundamentals();

        processStockPrices();

        processTreasuries();
    }
}
