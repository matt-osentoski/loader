package com.garagebandhedgefund.data.loaders.stockprices;

import com.garagebandhedgefund.data.dao.StockPriceDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Matt on 10/20/2014.
 */
public class CreateIndexesTasklet implements Tasklet {

    private StockPriceDAO stockPriceDAO;

    public StockPriceDAO getStockPriceDAO() {
        return stockPriceDAO;
    }

    public void setStockPriceDAO(StockPriceDAO stockPriceDAO) {
        this.stockPriceDAO = stockPriceDAO;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        stockPriceDAO.createStockPriceSymbolIndex();
        return RepeatStatus.FINISHED;
    }
}
