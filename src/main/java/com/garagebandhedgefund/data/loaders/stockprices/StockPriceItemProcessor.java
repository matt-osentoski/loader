package com.garagebandhedgefund.data.loaders.stockprices;

import com.garagebandhedgefund.trading.model.Stock;
import com.garagebandhedgefund.trading.model.StockPrice;

import org.springframework.batch.item.ItemProcessor;

/**
 * Created by Matt on 10/13/2014.
 */
public class StockPriceItemProcessor implements ItemProcessor<StockPrice, StockPrice> {

    private String symbol;
    private int stockId;

    @Override
    public StockPrice process(StockPrice stockPrice) throws Exception {
        Stock stock = new Stock();
        stock.setId(stockId);
        stockPrice.setSymbol(symbol);
        stockPrice.setPeriod("d");
        stockPrice.setStock(stock);
        return stockPrice;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
