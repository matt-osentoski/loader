package com.garagebandhedgefund.data.loaders.stockprices;

import com.garagebandhedgefund.trading.model.StockPrice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import javax.inject.Named;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Matt on 10/14/2014.
 */
public class StockPriceFieldSetMapper implements FieldSetMapper<StockPrice> {

    final static Logger logger = LoggerFactory.getLogger(StockPriceFieldSetMapper.class);

    @Override
    public StockPrice mapFieldSet(FieldSet fieldSet) throws BindException {
        StockPrice sp = new StockPrice();
        String priceDateStr = fieldSet.readString("priceDate");
        Date priceDate = null;
        try {
            priceDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(priceDateStr.trim());
        } catch (ParseException e) {
            logger.error(e.getMessage(),e);
        }

        sp.setPriceDate(priceDate);
        sp.setOpeningPrice(fieldSet.readDouble("openingPrice"));
        sp.setHighPrice(fieldSet.readDouble("highPrice"));
        sp.setLowPrice(fieldSet.readDouble("lowPrice"));
        sp.setClosingPrice(fieldSet.readDouble("closingPrice"));
        sp.setAdjClosingPrice(fieldSet.readDouble("adjClosingPrice"));
        sp.setVolume(fieldSet.readInt("volume"));
        return sp;
    }
}
