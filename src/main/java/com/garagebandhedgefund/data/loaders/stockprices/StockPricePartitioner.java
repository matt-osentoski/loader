package com.garagebandhedgefund.data.loaders.stockprices;

import com.garagebandhedgefund.data.dao.StockDAO;
import com.garagebandhedgefund.trading.model.Stock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Matt on 10/12/2014.
 */
@Named
public class StockPricePartitioner implements Partitioner {

    @Inject
    public StockDAO stockDAO;

    @Override
    public Map<String, ExecutionContext> partition(int i) {
        Map<String, ExecutionContext> result = new HashMap<String, ExecutionContext>();

        List<Stock> stocks = stockDAO.getStocks();
        for (Stock stock : stocks) {
            ExecutionContext value = new ExecutionContext();
            value.putString("symbol", stock.getSymbol());
            value.putString("stockId", Integer.toString(stock.getId()));
            result.put(stock.getSymbol(), value);
        }
        return result;
    }
}
