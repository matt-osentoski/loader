package com.garagebandhedgefund.data.loaders.stockfundamentals;

import com.garagebandhedgefund.data.dao.StockFundamentalsDAO;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * Created by Matt on 10/20/2014.
 */
public class TruncateStockFundamentalsTasklet implements Tasklet {

    private StockFundamentalsDAO stockFundamentalsDAO;

    public StockFundamentalsDAO getStockFundamentalsDAO() {
        return stockFundamentalsDAO;
    }

    public void setStockFundamentalsDAO(StockFundamentalsDAO stockFundamentalsDAO) {
        this.stockFundamentalsDAO = stockFundamentalsDAO;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        stockFundamentalsDAO.deleteAllStockFundamentals();
        return RepeatStatus.FINISHED;
    }
}
