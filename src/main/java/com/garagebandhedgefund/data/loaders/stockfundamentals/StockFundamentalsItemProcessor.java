package com.garagebandhedgefund.data.loaders.stockfundamentals;

import com.garagebandhedgefund.trading.model.Stock;
import com.garagebandhedgefund.trading.model.StockFundamentalData;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by Matt on 10/13/2014.
 */
public class StockFundamentalsItemProcessor implements ItemProcessor<StockFundamentalData, StockFundamentalData> {

    private String symbol;
    private int stockId;

    @Override
    public StockFundamentalData process(StockFundamentalData stockFundamentalData) throws Exception {
        Stock stock = new Stock();
        stock.setId(stockId);
        stockFundamentalData.setSymbol(symbol);
        stockFundamentalData.setStock(stock);
        return stockFundamentalData;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
