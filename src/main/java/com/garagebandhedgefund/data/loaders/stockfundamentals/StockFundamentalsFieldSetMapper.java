package com.garagebandhedgefund.data.loaders.stockfundamentals;

import com.garagebandhedgefund.trading.model.StockFundamentalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Matt on 10/14/2014.
 */
public class StockFundamentalsFieldSetMapper implements FieldSetMapper<StockFundamentalData> {

    final static Logger logger = LoggerFactory.getLogger(StockFundamentalsFieldSetMapper.class);

    @Override
    public StockFundamentalData mapFieldSet(FieldSet fieldSet) throws BindException {
        StockFundamentalData sf = new StockFundamentalData();


        String qtrEndDateStr = fieldSet.readString("quarter_end_date");
        Date qtrEndDate = null;
        try {
            qtrEndDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(qtrEndDateStr.trim());
        } catch (ParseException e) {
            logger.error(e.getMessage(),e);
        }

        sf.setQuarterEndDate(qtrEndDate);
        sf.setShares(parseFieldSetLong(fieldSet.readString("shares")));
        sf.setSharesSplitAdjusted(parseFieldSetLong(fieldSet.readString("shares_split_adj")));
        sf.setSplitFactor(fieldSet.readInt("split_factor"));
        sf.setAssets(parseFieldSetLong(fieldSet.readString("assets")));
        sf.setLiabilities(parseFieldSetLong(fieldSet.readString("liabilities")));
        sf.setShareholderEquity(parseFieldSetLong(fieldSet.readString("shareholder_equity")));
        sf.setNonControllingInterest(parseFieldSetLong(fieldSet.readString("non_controlling_interest")));
        sf.setPreferredEquity(parseFieldSetLong(fieldSet.readString("preferred_equity")));
        sf.setGoodwillAndTangibles(parseFieldSetLong(fieldSet.readString("goodwill_and_tangibles")));
        sf.setLongTermDebt(parseFieldSetLong(fieldSet.readString("long_term_debt")));
        sf.setRevenue(parseFieldSetLong(fieldSet.readString("revenue")));
        sf.setEarnings(parseFieldSetLong(fieldSet.readString("earnings")));
        sf.setEarningsAvailForCommonStockholders(parseFieldSetLong(fieldSet.readString("earnings_avail_for_comm_stkholders")));
        sf.setEpsBasic(parseFieldSetDouble(fieldSet.readString("eps_basic")));
        sf.setEpsDiluted(parseFieldSetDouble(fieldSet.readString("eps_diluted")));
        sf.setDividendPerShare(parseFieldSetDouble(fieldSet.readString("dividend_per_share")));
        sf.setPrice(parseFieldSetDouble(fieldSet.readString("price")));
        sf.setPriceHigh(parseFieldSetDouble(fieldSet.readString("price_high")));
        sf.setPriceLow(parseFieldSetDouble(fieldSet.readString("price_low")));
        sf.setPriceLow(parseFieldSetDouble(fieldSet.readString("price_low")));
        sf.setRoe(parseFieldSetDouble(fieldSet.readString("roe")));
        sf.setRoa(parseFieldSetDouble(fieldSet.readString("roa")));
        sf.setBookValueOfEquityPerShare(parseFieldSetDouble(fieldSet.readString("book_val_of_equity_per_share")));
        sf.setPbRatio(parseFieldSetDouble(fieldSet.readString("p_b_ratio")));
        sf.setPeRatio(parseFieldSetDouble(fieldSet.readString("p_e_ratio")));
        sf.setCumulativeDividends(parseFieldSetDouble(fieldSet.readString("cumulative_dividends")));
        sf.setDividendPayoutRatio(parseFieldSetDouble(fieldSet.readString("dividend_payout_ratio")));
        sf.setLongTermDebtToEquityRatio(parseFieldSetDouble(fieldSet.readString("long_term_debt_to_eqty_ratio")));
        sf.setEquityToAssetsRatio(parseFieldSetDouble(fieldSet.readString("equity_to_assets_ratio")));
        sf.setNetMargin(parseFieldSetDouble(fieldSet.readString("net_margin")));
        sf.setAssetTurnover(parseFieldSetDouble(fieldSet.readString("asset_turnover")));
        return sf;
    }

    private static long parseFieldSetLong(String inputStr) {
        long ret;
        if (inputStr == null || "".equals(inputStr.trim()) ||
                "None".equals(inputStr.trim()) || !isNumeric(inputStr)) {
            return 0;
        }

        try {
           ret = Long.parseLong(inputStr);
        } catch (NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
        return ret;
    }

    private static double parseFieldSetDouble(String inputStr) {
        double ret;
        if (inputStr == null || "".equals(inputStr.trim()) ||
                "None".equals(inputStr.trim()) || !isNumeric(inputStr)) {
            return 0;
        }

        try {
            ret = Double.parseDouble(inputStr);
        } catch (NumberFormatException e) {
            logger.error(e.getMessage(), e);
            return 0;
        }
        return ret;
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}
