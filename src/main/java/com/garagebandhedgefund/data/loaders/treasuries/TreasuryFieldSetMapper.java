package com.garagebandhedgefund.data.loaders.treasuries;

import com.garagebandhedgefund.data.exceptions.InvalidTreasuryException;
import com.garagebandhedgefund.trading.model.Treasury;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Matt on 10/18/2014.
 */
public class TreasuryFieldSetMapper implements FieldSetMapper<Treasury> {
    @Override
    public Treasury mapFieldSet(FieldSet fieldSet) throws BindException {
        Treasury t = new Treasury();
        String dateStr = fieldSet.readString("timePeriod");
        Date treasuryDate = null;
        try {
            treasuryDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(dateStr.trim());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String c1month = fieldSet.readString("c1month");
        String c3month = fieldSet.readString("c3month");
        String c6month = fieldSet.readString("c6month");
        String c1year = fieldSet.readString("c1year");
        String c2year = fieldSet.readString("c2year");
        String c3year = fieldSet.readString("c3year");
        String c5year = fieldSet.readString("c5year");
        String c7year = fieldSet.readString("c7year");
        String c10year = fieldSet.readString("c10year");
        String c20year = fieldSet.readString("c20year");
        String c30year = fieldSet.readString("c30year");


        t.setTimePeriod(treasuryDate);
        t.setC1month(parseTreasuryRate(c1month));
        t.setC3month(parseTreasuryRate(c3month));
        t.setC6month(parseTreasuryRate(c6month));
        t.setC1year(parseTreasuryRate(c1year));
        t.setC2year(parseTreasuryRate(c2year));
        t.setC3year(parseTreasuryRate(c3year));
        t.setC5year(parseTreasuryRate(c5year));
        t.setC7year(parseTreasuryRate(c7year));
        t.setC10year(parseTreasuryRate(c10year));
        t.setC20year(parseTreasuryRate(c20year));
        t.setC30year(parseTreasuryRate(c30year));

        return t;
    }

    private double parseTreasuryRate(String rate) {
        double ret = 0;

        if ("".equals(rate.trim())) {
            return 0;
        }

        if ("ND".equals(rate.trim())) {
            return -666;
        }
        ret = Double.parseDouble(rate);

        return ret;
    }
}
