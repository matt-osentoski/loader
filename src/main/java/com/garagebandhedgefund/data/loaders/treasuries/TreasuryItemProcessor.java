package com.garagebandhedgefund.data.loaders.treasuries;

import com.garagebandhedgefund.data.exceptions.InvalidTreasuryException;
import com.garagebandhedgefund.trading.model.Treasury;
import org.springframework.batch.item.ItemProcessor;

import javax.inject.Named;

/**
 * Created by Matt on 10/18/2014.
 */
@Named
public class TreasuryItemProcessor implements ItemProcessor<Treasury, Treasury> {

    @Override
    public Treasury process(Treasury treasury) throws Exception {

        if (treasury.getC1month() == -666 || treasury.getC3month() == -666 || treasury.getC6month() == -666 ||
                treasury.getC1year() == -666 || treasury.getC2year() == -666 || treasury.getC3year() == -666 ||
                treasury.getC5year() == -666 || treasury.getC7year() == -666 || treasury.getC10year() == -666 ||
                treasury.getC20year() == -666 || treasury.getC30year() == -666) {
            throw new InvalidTreasuryException("Skipping " + treasury.getTimePeriod());
        }

        return treasury;
    }
}
