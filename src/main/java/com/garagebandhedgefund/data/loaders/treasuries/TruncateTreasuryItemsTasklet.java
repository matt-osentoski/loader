package com.garagebandhedgefund.data.loaders.treasuries;

import com.garagebandhedgefund.data.dao.TreasuryDAO;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Matt on 10/20/2014.
 */
public class TruncateTreasuryItemsTasklet implements Tasklet {

    private TreasuryDAO treasuryDAO;

    public TreasuryDAO getTreasuryDAO() {
        return treasuryDAO;
    }

    public void setTreasuryDAO(TreasuryDAO treasuryDAO) {
        this.treasuryDAO = treasuryDAO;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        treasuryDAO.deleteAllTreasuryEntries();
        return RepeatStatus.FINISHED;
    }
}
