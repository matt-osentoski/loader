package com.garagebandhedgefund.data.exceptions;

/**
 * Created by Matt on 10/18/2014.
 */
public class InvalidTreasuryException extends RuntimeException {
    public InvalidTreasuryException() {super();}
    public InvalidTreasuryException(String message) {super(message);}
    public InvalidTreasuryException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalidTreasuryException(Throwable cause) {super(cause);}
}
