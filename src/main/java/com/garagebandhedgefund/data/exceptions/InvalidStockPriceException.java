package com.garagebandhedgefund.data.exceptions;

/**
 * Created by Matt on 10/12/2014.
 */
public class InvalidStockPriceException extends RuntimeException {
    public InvalidStockPriceException() {super();}
    public InvalidStockPriceException(String message) {super(message);}
    public InvalidStockPriceException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalidStockPriceException(Throwable cause) {super(cause);}
}
