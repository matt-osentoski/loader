package com.garagebandhedgefund.data.exceptions;

/**
 * Created by Matt on 10/12/2014.
 */
public class InvalidStockFundamentalsException extends RuntimeException {
    public InvalidStockFundamentalsException() {super();}
    public InvalidStockFundamentalsException(String message) {super(message);}
    public InvalidStockFundamentalsException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalidStockFundamentalsException(Throwable cause) {super(cause);}
}
