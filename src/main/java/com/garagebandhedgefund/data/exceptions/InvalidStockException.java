package com.garagebandhedgefund.data.exceptions;

/**
 * Created by Matt on 10/5/2014.
 */
public class InvalidStockException extends RuntimeException {
    public InvalidStockException() {super();}
    public InvalidStockException(String message) {super(message);}
    public InvalidStockException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalidStockException(Throwable cause) {super(cause);}
}
