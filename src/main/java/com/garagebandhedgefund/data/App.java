package com.garagebandhedgefund.data;

import com.garagebandhedgefund.data.loaders.LoadProcessor;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Loader entry point
 *
 */
public class App {

    final static Logger logger = LoggerFactory.getLogger(App.class);

    public static void main( String[] args ) {

        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});

        Options options = getOptions();
        CommandLineParser parser = new BasicParser();
        HelpFormatter formatter = new HelpFormatter();
        try {
            CommandLine cmd = parser.parse(options, args);
            if (validArgs(cmd)) {
                LoadProcessor proc = (LoadProcessor)context.getBean("loadProcessor");
                proc.process(cmd.getOptionValue("L"));
            } else {
                formatter.printHelp("App", options);
            }
        } catch (ParseException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private static boolean validArgs(CommandLine cmd) {
        boolean val = true;
        if (!cmd.hasOption("L") || cmd.getOptionValue("L") == null) {
            return false;
        }

        String loaderValue = cmd.getOptionValue("L");
        if (!Constants.LOADER_STOCKS.equals(loaderValue) &&
                !Constants.LOADER_STOCK_FUNDAMENTALS.equals(loaderValue) &&
                !Constants.LOADER_STOCK_PRICES.equals(loaderValue) &&
                !Constants.LOADER_TREASURIES.equals(loaderValue) &&
                !Constants.LOADER_ALL.equals(loaderValue)) {
            return false;
        }

        return val;
    }

    private static Options getOptions() {
        Options options = new Options();
        options.addOption("L", true, "[Required] The loaders to use: " +
                "(" + Constants.LOADER_STOCKS + ", " +
                Constants.LOADER_STOCK_FUNDAMENTALS + ", " +
                Constants.LOADER_STOCK_PRICES + ", " +
                Constants.LOADER_TREASURIES + ", " +
                Constants.LOADER_ALL + ")");
        return options;
    }
}
