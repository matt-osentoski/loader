package com.garagebandhedgefund.data;

/**
 * Created with IntelliJ IDEA.
 * User: mosentos
 * Date: 9/19/14
 * Time: 12:16 PM
 */
public class Constants {
    public final static String LOADER_STOCKS = "stock";
    public final static String LOADER_STOCK_FUNDAMENTALS = "stockFundamentals";
    public final static String LOADER_STOCK_PRICES = "stockPrice";
    public final static String LOADER_TREASURIES = "treasury";
    public final static String LOADER_ALL = "all";

    public final static String LOAD_NASDAQ_STOCKS_STEP = "load-nasdaq-stocks-step";
    public final static String LOAD_NYSE_STOCKS_STEP = "load-nyse-stocks-step";

    public final static String EXCHANGE_NASDAQ = "NASDAQ";
    public final static String EXCHANGE_NYSE = "NYSE";

    public static final String STOCKS_COLLECTION = "stocks";
    public static final String STOCK_PRICES_COLLECTION = "stock_prices";
    public static final String TREASURIES_COLLECTION = "treasuries";

    public static enum LoaderEnum {
        STOCK,
        STOCKFUNDAMENTALS,
        STOCKPRICE,
        TREASURY,
        ALL
    }
}
