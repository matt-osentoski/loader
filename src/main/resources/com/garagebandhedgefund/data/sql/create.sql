﻿-- To avoid constraint problems during this script, turn foreign
-- key checks off.
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS stocks;
CREATE TABLE stocks (
	id int not null auto_increment,
	symbol varchar(50),
	name varchar(255),
	description text,
	market_cap bigint,
	ipo_year varchar(20),
	sector varchar(255),
	industry varchar(255),
	stock_index varchar(50),
	primary key (id)
) ENGINE = InnoDB;

CREATE INDEX idx_stocks_symbol_666 ON stocks (symbol(6));

DROP TABLE IF EXISTS stock_prices;
CREATE TABLE stock_prices (
	id int not null auto_increment,
	stock_id int,
	symbol varchar(50),  -- Denormalize the symbol for convenience
	price_date datetime, 
	opening_price decimal(10,2), 
	high_price decimal(10,2), 
	low_price decimal(10,2), 
	closing_price decimal(10,2),
	adj_closing_price decimal(10,2), 
	period varchar(1),
	volume int,
	primary key (id),
	FOREIGN KEY(stock_id) REFERENCES stocks(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB;

-- Quarterly fundamental stock data
DROP TABLE IF EXISTS stock_fundamental_data;
CREATE TABLE stock_fundamental_data (
	id int not null auto_increment,
	stock_id int,
	symbol varchar(50),  -- Denormalize the symbol for convenience
	quarter_end_date datetime,
	shares bigint,
	shares_split_adj bigint,
	split_factor int,
	assets bigint,
	liabilities bigint,
	shareholder_equity bigint,
	non_controlling_interest bigint,
	preferred_equity bigint,
	goodwill_and_tangibles bigint,
	long_term_debt bigint,
	revenue bigint,
	earnings bigint,
	earnings_avail_for_comm_stkholders bigint,
	eps_basic decimal(10,2),
	eps_diluted decimal(10,2),
	dividend_per_share decimal(10,4),
	price decimal(10,2),
	price_high decimal(10,2),
	price_low decimal(10,2),
	roe decimal(10,4),
	roa decimal(10,4),
	book_val_of_equity_per_share decimal(10,2),
	p_b_ratio decimal(10,4),
	p_e_ratio decimal(10,4),
	cumulative_dividends decimal(10,2),
	dividend_payout_ratio decimal(10,4),
	long_term_debt_to_eqty_ratio decimal(10,4),
	equity_to_assets_ratio decimal(10,4),
	net_margin decimal(10,4),
	asset_turnover decimal(10,4),
	primary key (id),
	FOREIGN KEY(stock_id) REFERENCES stocks(id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB;

-- Historical Treasury rates
DROP TABLE IF EXISTS treasuries;
CREATE TABLE treasuries (
	id int not null auto_increment,
	time_period datetime,
	1_month decimal(10,2),  -- Values are percents
	3_month decimal(10,2),  -- Values are percents
	6_month decimal(10,2),  -- Values are percents
	1_year decimal(10,2),  -- Values are percents
	2_year decimal(10,2),  -- Values are percents
	3_year decimal(10,2),  -- Values are percents
	5_year decimal(10,2),  -- Values are percents
	7_year decimal(10,2),  -- Values are percents
	10_year decimal(10,2),  -- Values are percents
	20_year decimal(10,2),  -- Values are percents
	30_year decimal(10,2),  -- Values are percents
	primary key (id)
) ENGINE = InnoDB;


-- Create an index to increase performance when searching by symbol

-- CREATE INDEX idx_stock_prices_symbol ON stock_prices (symbol);
-- CREATE INDEX idx_stock_prices_symbol_period ON stock_prices (symbol, period);

