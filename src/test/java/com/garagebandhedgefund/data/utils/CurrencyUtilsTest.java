package com.garagebandhedgefund.data.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Matt on 12/31/2015.
 */
public class CurrencyUtilsTest {

    @Test
    public void testParseMarketCapString() throws Exception {
        assertEquals(1850000000, CurrencyUtils.parseMarketCapString("$1.85B"), 0.0001);
        assertEquals(1850000, CurrencyUtils.parseMarketCapString("$1.85M"), 0.0001);
        assertEquals(671500, CurrencyUtils.parseMarketCapString("$671,500"), 0.0001);
        assertEquals(671500, CurrencyUtils.parseMarketCapString("671500"), 0.0001);
        assertEquals(0, CurrencyUtils.parseMarketCapString("n/a"), 0.0001);
        assertEquals(0, CurrencyUtils.parseMarketCapString("0"), 0.0001);
        assertEquals(0, CurrencyUtils.parseMarketCapString(""), 0.0001);

        //negative testing
        assertNotEquals(1850000001, CurrencyUtils.parseMarketCapString("$1.85B"), 0.0001);
        assertNotEquals(1850001, CurrencyUtils.parseMarketCapString("$1.85M"), 0.0001);
        assertNotEquals(671501, CurrencyUtils.parseMarketCapString("$671,500"), 0.0001);
        assertNotEquals(671501, CurrencyUtils.parseMarketCapString("671500"), 0.0001);
        assertNotEquals(1, CurrencyUtils.parseMarketCapString("n/a"), 0.0001);
        assertNotEquals(1, CurrencyUtils.parseMarketCapString("0"), 0.0001);
        assertNotEquals(1, CurrencyUtils.parseMarketCapString(""), 0.0001);
    }
}